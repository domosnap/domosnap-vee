# Kafka 

## Install Kafka and launch
- Launch Zookeeper
```bin/zookeeper-server-start.sh config/zookeeper.properties```

- Launch Kafka
```bin/kafka-server-start.sh config/server.properties```

## Compile and launch app

- Compile
```mvn package```

- Launch
```mvn exec:java@Main```


Need to lunch probe service to push event in topic: domosnap-event-topic
After lunch FilterPowerCounter = get data from domosnap-event-topic, filter powercounter with active power and push them on topic:domosnap-power-counter
After lunch the VEE Detecting or th PowerCounterAggegation!

bin/kafka-console-consumer.sh --bootstrap-server 192.168.1.63:31338 --topic domosnap-power-counter --from-beginning

## Install Conduktor
docker run --rm --pull always -p 8080:8080 --add-host=host.docker.internal:host-gateway --mount "source=conduktor_data,target=/var/conduktor" conduktor/conduktor-platform:latest
After a few minutes, Conduktor will be available at http://localhost:8080



User: admin@conduktor.io
Password: admin

Configure cluster...

## Install KSQL:
https://ksqldb.io/quickstart-standalone-tarball.html#quickstart-content

lunch the ksql server in k8s => see raspberry-infra project /kafka/ksql
Lunch the cli:
sudo bin/ksql http://192.168.1.60:30635

Power:

CREATE OR REPLACE STREAM domoevent (who VARCHAR, "where" VARCHAR KEY, "when" TIMESTAMP, what STRUCT<ACTIVE_POWER STRING>) WITH (kafka_topic='domosnap-event-topic', value_format='json', partitions=1);

CREATE OR REPLACE STREAM power AS SELECT "where", AS_VALUE("where") AS keywhere, "when", CAST(SUBSTRING(what->ACTIVE_POWER,19) AS INT) AS ACTIVE_POWER FROM domoevent WHERE what->ACTIVE_POWER IS NOT NULL EMIT CHANGES;

SELECT * FROM power WHERE ACTIVE_POWER > 4000;

 
Temperature:
CREATE OR REPLACE STREAM temperature (who VARCHAR, "where" VARCHAR KEY,  "when" TIMESTAMP, what STRUCT<MEASURE_TEMPERATURE DOUBLE>) WITH (kafka_topic='domosnap-event-topic', value_format='json', partitions=1);

SELECT * FROM temperature WHERE what->MEASURE_TEMPERATURE > 10;

CREATE OR REPLACE STREAM temperature2 AS SELECT "where", AS_VALUE("where") AS keywhere, "when", what->MEASURE_TEMPERATURE FROM temperature WHERE what->MEASURE_TEMPERATURE IS NOT
NULL EMIT CHANGES;

SELECT * FROM temperature WHERE WHAT->MEASURE_TEMPERATURE IS NOT NULL;

## Install influxdb
docker run --name influxdb -p 8086:8086 influxdb:2.6.1

ensuite lancer le telegrah (d'abord il faut configurer le plugin kafka... = voir le fichier telegraph_kafka_configuration)

ensuite on peut faire des dashboard!

https://octoperf.com/blog/2019/09/19/kraken-kubernetes-influxdb-grafana-telegraf/#expose-a-deployment-as-a-service


from(bucket: "domosnap")
|> range(start: v.timeRangeStart, stop: v.timeRangeStop)
|> filter(fn: (r) => r["_measurement"] == "scs://12345@192.168.1.35:20000/5101" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5111" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5112" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5113" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5114" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5116" or r["_measurement"] == "scs://12345@192.168.1.35:20000/5115")
|> filter(fn: (r) => r["_field"] == "ACTIVE_POWER")
|> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
|> yield(name: "mean")




## Test de performance

un gros message = 150 octets
550'000 compteurs = 550'000 * 150 octets / seconde => 55 000 000 + 27 500 000 => 82 500 000 octet soit 80 566 Ko soit 78,7 Mo /s!!!

Donc 80Mo/s soit 4720,7Mo/m soit 283241Mo/h soit 6797790.5Mo/j soit 203933715.8Mo/mois
Donc 80Mo/s soit 4,6 Go/m   soit 276,60Go/h soit 6,4To/j       soit 194,48TMois

Actuellement à la maison: 5 compteur => 
5* 150 oct =


Faire un test toutes les 5 secondes envoyer 550 000 message!