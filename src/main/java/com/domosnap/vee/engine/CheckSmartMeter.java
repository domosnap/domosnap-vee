package com.domosnap.vee.engine;

import com.domosnap.vee.rule.PowerLivenessRule;
import com.domosnap.vee.rule.TemperatureAlertRule;
import com.domosnap.vee.engine.core.Rule;
import com.domosnap.vee.rule.WaterConsumptionRule;
import com.domosnap.vee.projection.PowerCounterKtable;

import java.util.*;

public class CheckSmartMeter {

    private static List<String> addressToChek = new ArrayList<>();
    private static List<String> waterToChek = new ArrayList<>();

    static {
        // ICI une liste des devices à checker MAIS si de nouveau device envoi des données je les ajoutes dans la liste et il sont aussi checker ensuite! A voir si comportement voulu
        addressToChek.add("scs://12345@192.168.1.35:20000/5111"); // TODO je stocke les value ici = pas de tolérance des pannes... peut mieux faire..
        addressToChek.add("scs://12345@192.168.1.35:20000/5112");
        addressToChek.add("scs://12345@192.168.1.35:20000/5113");
        addressToChek.add("scs://12345@192.168.1.35:20000/5114");
        addressToChek.add("scs://12345@192.168.1.35:20000/5115");
        addressToChek.add("scs://12345@192.168.1.35:20000/5116");

        waterToChek.add("scs://12345@192.168.1.35:20000/5101");
    }
    public static void main(String[] args) {

        Engine engine = new Engine();
        Properties p = new Properties();
        p.put(PowerLivenessRule.ADDRESS_TO_CHECK, addressToChek);
        Rule rule = new PowerLivenessRule("power_liveness", "192.168.1.63:31338", "domosnap-event-topic", p);
        engine.add(rule);

        Properties p2 = new Properties();
        p2.put(TemperatureAlertRule.ALERT_INFO, 23);
        p2.put(TemperatureAlertRule.ALERT_WARNING, 30);
        p2.put(TemperatureAlertRule.ALERT_ERROR, 37);
        Rule rule2 = new TemperatureAlertRule("temperature_alert", "192.168.1.63:31338", "domosnap-event-topic", p2);
        engine.add(rule2);

        Rule rule3 = new PowerCounterKtable("PowerConsumerToKTable", "192.168.1.63:31338", "domosnap-event-topic", p2);
        engine.add(rule3);

        Properties p4 = new Properties();
        p4.put(WaterConsumptionRule.ADDRESS_TO_CHECK, waterToChek);
        Rule rule4 = new WaterConsumptionRule("water_consumption", "192.168.1.63:31338", "domosnap-event-topic", p4);
        engine.add(rule4);

        engine.start();
        System.out.println("VEE Engine started.");

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


}
