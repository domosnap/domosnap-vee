package com.domosnap.vee.engine;

import com.domosnap.vee.engine.core.Rule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Engine {

    private final List<Rule> rules = new ArrayList<>();
    private boolean running = false;

    private final ExecutorService service = Executors.newSingleThreadExecutor();

    public synchronized void start() {
        service.submit(() -> {
                rules.forEach(rule -> rule.getRule().start());
                running = true;
        });
    }

    public synchronized void stop() {
        rules.forEach(rule -> rule.getRule().close());
        service.shutdown();
        running = false;
    }

    public synchronized void add(Rule rule) {
        rules.add(rule);
        if (running) {
            rule.getRule().start();
        }
    }

    public synchronized void remove(Rule rule) {
        if (rules.remove(rule)) {
            rule.getRule().close();
        }

    }

}
