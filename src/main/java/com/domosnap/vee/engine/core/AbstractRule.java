package com.domosnap.vee.engine.core;

import java.util.Properties;

public abstract class AbstractRule implements Rule {

    private String name;
    private String bootstrapServer;
    private String topic;


    public AbstractRule(String name, String bootstrapServer, String topic, Properties settings) {
        this.name = name;
        this.topic = topic;
        this.bootstrapServer = bootstrapServer;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBootstrapServer() {
        return bootstrapServer;
    }

    @Override
    public String getTopic() {
        return topic;
    }

}
