package com.domosnap.vee.engine.core;

import org.apache.kafka.streams.KafkaStreams;

public interface Rule {

    public String getName();

    public String getBootstrapServer();

    public String getTopic();

    KafkaStreams getRule();
}
