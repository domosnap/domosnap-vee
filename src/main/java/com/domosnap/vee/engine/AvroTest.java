//package com.domosnap.vee.devicestatus.rule;
//
//import com.domosnap.model.device.PowerCounter;
//import org.apache.avro.file.DataFileWriter;
//import org.apache.avro.io.DatumWriter;
//import org.apache.avro.specific.SpecificDatumWriter;
//
//import java.io.File;
//import java.io.IOException;
//
//public class AvroTest {
//
//    public static void main(String[] args) {
//        PowerCounter pc1 = new PowerCounter(10L, 20000);
//        PowerCounter pc2 = new PowerCounter(20L, 40000);
//        PowerCounter pc3 = new PowerCounter(30L, 60000);
//        DatumWriter<PowerCounter> userDatumWriter = new SpecificDatumWriter<>(PowerCounter.class);
//        DataFileWriter<PowerCounter> dataFileWriter = new DataFileWriter<PowerCounter>(userDatumWriter);
//        try {
//            dataFileWriter.create(pc1.getSchema(), new File("users.avro"));
//            dataFileWriter.append(pc1);
//            dataFileWriter.append(pc2);
//            dataFileWriter.append(pc3);
//
//            System.out.println(pc3);
//            dataFileWriter.close();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
//    }
//}
