package com.domosnap.vee.projection;

import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.model.core.interaction.Event;
import com.domosnap.model.devices.power.PowerCounter;
import com.domosnap.model.devices.power.PowerCounterStatus;
import com.domosnap.model.states.WattState;
import com.domosnap.vee.engine.core.AbstractRule;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Instant;
import java.util.HashMap;
import java.util.Properties;

public class PowerCounterKtable extends AbstractRule  {

    private final KafkaStreams rule;

    public PowerCounterKtable(String name, String bootstrapServer, String topic, Properties settings) {
        super(name, bootstrapServer, topic, settings);
        rule = getPowerKtable();
    }
    public static void main(String[] args) {
        KafkaStreams kafkaStreams = new PowerCounterKtable("PowerConsumerToKTable", "192.168.1.63:31338", "domosnap-event-topic", null).getPowerKtable();
        kafkaStreams.start();
        try {
            Thread.sleep(350000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Shutting down the Yelling APP now");
        kafkaStreams.close();
    }
    public KafkaStreams getPowerKtable() {

        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServer());
        StreamsConfig streamsConfig = new StreamsConfig(props);

        StreamsBuilder builder = new StreamsBuilder();


        Serde<String> stringSerde = Serdes.String();

        builder.stream(getTopic(), Consumed.with(stringSerde, stringSerde))
                // filter PowerCounter
                .filter( (String key, String value) -> {
                    JsonObject event = new JsonParser().parse(value).getAsJsonObject();
                    String who = event.get("who").getAsString();
                    if (who == null) {
                        return false;
                    }
                    return DeviceMappingRegistry.getMapping(com.domosnap.engine.device.counter.PowerCounter.class).equals(who);
                })
                .mapValues((value) -> { // map from json to avro model
                        // {"when":1673274073785,"who":"PowerCounter","where":"scs://*****@192.168.1.35:20000/5116","what":[{"ACTIVE_POWER":"0","type":"WattState"}]}
                        JsonParser jp = new JsonParser();
                        JsonObject event = jp.parse(value).getAsJsonObject();
                        long when = event.get("when").getAsLong();
                        String key = event.get("where").getAsString();
                        JsonArray whatList = event.getAsJsonArray("what");

                        HashMap<CharSequence, Object> listState = new HashMap<>();
                        for (JsonElement what : whatList) {
                            if (what.getAsJsonObject().get("ACTIVE_POWER") != null) {
                                listState.put(PowerCounterStatus.load.name(), new WattState(what.getAsJsonObject().get("ACTIVE_POWER").getAsInt()));
                            }
                        }

//                        System.out.println("La valeur est mappé: " + value);
                        return new Event("PowerCounter", Instant.ofEpochMilli(when), key, listState);
                    }
                )
                .groupByKey(Grouped.as("pcgroup"))
                .aggregate(() -> {
                        return new PowerCounter(true, "", new WattState(0), new WattState(0));
                    }
                    ,(key, value, valueAggregated) -> {
//                            System.out.println("Compteur [" + key + "] mis à jour avec la valeur: " + value.toString());
                            valueAggregated.setUri(key);
                            if (value.getStatesList().get(PowerCounterStatus.load.name()) != null) {
                                WattState load = (WattState) value.getStatesList().get(PowerCounterStatus.load.name());
                                valueAggregated.setLoad(load);
                                valueAggregated.setIndex(new WattState(valueAggregated.getIndex().getValue() + ((WattState) value.getStatesList().get(PowerCounterStatus.load.name())).getValue()));
                            }
                            return valueAggregated;
                },
                Materialized.<String, PowerCounter, KeyValueStore<Bytes,byte[]>>as("domosnap-vee-powercounter-keyvalueStore")
                        .withKeySerde(Serdes.String())
                        .withValueSerde(com.domosnap.vee.serdes.Serdes.PowerCounterSerde()))
                .toStream(Named.as("domosnap-vee-powercounter"))
                .to("domosnap-vee-powercounter")
                ;

        return new KafkaStreams(builder.build(), streamsConfig);
    }


    @Override
    public KafkaStreams getRule() {
        return rule;
    }
}
