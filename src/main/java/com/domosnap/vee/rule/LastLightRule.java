package com.domosnap.vee.rule;

import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.model.core.interaction.Event;
import com.domosnap.model.devices.light.Light;
import com.domosnap.model.devices.light.LightStatus;
import com.domosnap.model.devices.power.PowerCounter;
import com.domosnap.model.devices.power.PowerCounterStatus;
import com.domosnap.model.states.WattState;
import com.domosnap.vee.engine.core.AbstractRule;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Instant;
import java.util.HashMap;
import java.util.Properties;

public class LastLightRule extends AbstractRule  {

    private final KafkaStreams rule;

    public LastLightRule(String name, String bootstrapServer, String topic, Properties settings) {
        super(name, bootstrapServer, topic, settings);
        rule = getPowerKtable();
    }
    public KafkaStreams getPowerKtable() {

        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServer());
        StreamsConfig streamsConfig = new StreamsConfig(props);

        StreamsBuilder builder = new StreamsBuilder();


        Serde<String> stringSerde = Serdes.String();

        builder.stream(getTopic(), Consumed.with(stringSerde, stringSerde))
                // filter PowerCounter
                .filter( (String key, String value) -> {
                    JsonObject event = new JsonParser().parse(value).getAsJsonObject();
                    String who = event.get("who").getAsString();
                    if (who == null) {
                        return false;
                    }
                    return DeviceMappingRegistry.getMapping(com.domosnap.engine.device.light.Light.class).equals(who);
                })
                .mapValues((value) -> { // map from json to avro model
                        // {"when":1673274073785,"who":"PowerCounter","where":"scs://*****@192.168.1.35:20000/5116","what":[{"ACTIVE_POWER":"0","type":"WattState"}]}
                        JsonParser jp = new JsonParser();
                        JsonObject event = jp.parse(value).getAsJsonObject();
                        long when = event.get("when").getAsLong();
                        String key = event.get("where").getAsString();
                        JsonArray whatList = event.getAsJsonArray("what");

                        HashMap<CharSequence, Object> listState = new HashMap<>();
                        for (JsonElement what : whatList) {
                            if (what.getAsJsonObject().get("STATUS") != null) {
                                listState.put(LightStatus.status.name(), what.getAsJsonObject().get("STATUS").getAsBoolean());
                            }
                        }

//                        System.out.println("La valeur est mappé: " + value);
                        return new Event("Light", Instant.ofEpochMilli(when), key, listState);
                    }
                )
                .groupByKey()
//                .windowedBy(Window.)
                .aggregate(() -> {
                        return new Light();
                    }
                    ,(key, value, valueAggregated) -> {
//                            System.out.println("Compteur [" + key + "] mis à jour avec la valeur: " + value.toString());

                            if (value.getStatesList().get(LightStatus.status.name()) != null && !((Boolean)value.getStatesList().get(LightStatus.status.name()))) { // On éteins la lumière
                                valueAggregated.setUri(key);
                                valueAggregated.setStatus((Boolean) value.getStatesList().get(LightStatus.status.name()));
                            }
                            return valueAggregated;
                },
                Materialized.<String, Light, KeyValueStore<Bytes,byte[]>>as("domosnap-vee-lastLight")
                        .withKeySerde(Serdes.String())
                        .withValueSerde(com.domosnap.vee.serdes.Serdes.LightSerde()))
                .toStream(Named.as("domosnap-vee-lastLight"))
                .to("domosnap-vee-LastLight")
                ;

        return new KafkaStreams(builder.build(), streamsConfig);
    }

    @Override
    public KafkaStreams getRule() {
        return rule;
    }
}
