package com.domosnap.vee.rule;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.heating.HeatingZone;
import com.domosnap.vee.engine.core.AbstractRule;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.Record;

import java.util.Properties;

public class TemperatureAlertRule extends AbstractRule {

    private final KafkaStreams rule;

    public static final String ALERT_INFO = "alert.info";
    public static final String ALERT_WARNING = "alert.warning";
    public static final String ALERT_ERROR = "alert.error";
    public TemperatureAlertRule(String name, String bootstrapServer, String topic, Properties settings){

        super(name, bootstrapServer, topic, settings);

        Object alert_info_obj = settings.getOrDefault(ALERT_INFO, 20);
        Object alert_warning_obj = settings.getOrDefault(ALERT_WARNING, 27);
        Object alert_error_obj = settings.getOrDefault(ALERT_ERROR, 37);

        int alert_info, alert_warning, alert_error;

        if (!(alert_info_obj instanceof Integer) || !(alert_warning_obj instanceof Integer) || !(alert_error_obj instanceof Integer)) {
            throw new RuntimeException("Bad parameters.");
        } else {
            alert_info = (Integer) alert_info_obj;
            alert_warning = (Integer) alert_warning_obj;
            alert_error = (Integer) alert_error_obj;
        }

        final Class<? extends Device> type = HeatingZone.class;
        final String state = HeatingZone.HeatingZoneStateName.MEASURE_TEMPERATURE.name();


        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServer());
        StreamsConfig streamsConfig = new StreamsConfig(props);

        StreamsBuilder builder = new StreamsBuilder();

        Serde<String> stringSerde = Serdes.String();
        builder.stream(getTopic(), Consumed.with(stringSerde, stringSerde))
                .filter( (String key, String value) -> { // filter Temperature
                    JsonParser jp = new JsonParser();
                    JsonObject event = jp.parse(value).getAsJsonObject();
                    String who = event.get("who").getAsString();
                    if(DeviceMappingRegistry.getMapping(type).equals(who)) {
                        JsonArray whatList = event.getAsJsonArray("what");
                        for (JsonElement what : whatList) {
                            return what.getAsJsonObject().get(state) != null; // We get a measure temperature!
                        }
                    }
                    return false;
                })
                .process( () -> {
                            return new Processor<String, String, String, String>() {
                                @Override
                                public void process(Record<String, String> record) {
                                    JsonParser jp = new JsonParser();
                                    JsonObject event = jp.parse(record.value()).getAsJsonObject();
                                    JsonArray whatList = event.getAsJsonArray("what");
                                        for (JsonElement what : whatList) {
                                            if (what.getAsJsonObject().get(state) != null) {
                                                double temp = Double.parseDouble(what.getAsJsonObject().get(HeatingZone.HeatingZoneStateName.MEASURE_TEMPERATURE.name()).getAsString());
                                                if (temp > alert_error) {
                                                    System.out.println("La temperature mesurée par le device " + record.key() + " est supérieure à " + alert_error + "°.");
                                                } else if (temp > alert_warning) {
                                                    System.out.println("La temperature mesurée par le device " + record.key() + " est supérieure à " + alert_warning + "°.");
                                                } else if (temp > alert_info) {
                                                    System.out.println("La temperature mesurée par le device " + record.key() + " est supérieure à " + alert_info_obj + "°.");
                                                }
                                            }
                                        }

                                }
                            };
                        }
                        )
        ;

        rule = new KafkaStreams(builder.build(), streamsConfig);
    }


    public KafkaStreams getRule() {
        return rule;
    }



    public static void main(String[] args) {
        KafkaStreams kafkaStreams = new TemperatureAlertRule("temperatureRule", "192.168.1.63:31338", "domosnap-event-topic", null).getRule();
        kafkaStreams.start();
        try {
            Thread.sleep(350000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Shutting down the Yelling APP now");
        kafkaStreams.close();
    }
}
