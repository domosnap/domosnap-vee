package com.domosnap.vee.rule;

import com.domosnap.engine.Log;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.vee.engine.core.AbstractRule;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.api.ContextualProcessor;
import org.apache.kafka.streams.processor.api.ProcessorContext;
import org.apache.kafka.streams.processor.api.Record;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.time.Duration;
import java.util.*;

public class PowerLivenessRule extends AbstractRule {

    private static final Log log = new Log(PowerLivenessRule.class.getName());
    private final KafkaStreams rule;
    public static final String ADDRESS_TO_CHECK = "address.to.check";
    private final long checkDuration = 5_000; // TODO parameter
    private final long durationWithNoNews = 15_000; // TODO parameter

    public PowerLivenessRule(String name, String bootstrapServer, String topic, Properties settings) {

        // TODO refaire le meme basé sur le timestamp => différent de il n'y pas d'info de lissé les données!!!
        super(name, bootstrapServer, topic, settings);

        Map<String, Long> addressToCheck2 = new HashMap<>();
        Object addressToCheck = settings.getOrDefault(ADDRESS_TO_CHECK, new ArrayList<>());

        if (!(addressToCheck instanceof ArrayList)) {
            throw new RuntimeException("Bad parameter [" + ADDRESS_TO_CHECK + "]");
        } else {
            ((List<String>) addressToCheck).forEach( address -> addressToCheck2.put(address, 0L) );
        }

        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServer());
        StreamsConfig streamsConfig = new StreamsConfig(props);

        StreamsBuilder builder = new StreamsBuilder();


        String storeName = name + "_store";
        KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore(storeName);
        StoreBuilder<KeyValueStore<String, String>> storeBuilder = Stores.keyValueStoreBuilder(storeSupplier,
                Serdes.String(),
                Serdes.String());
        builder.addStateStore(storeBuilder);

        Serde<String> stringSerde = Serdes.String();

        builder.stream(getTopic(), Consumed.with(stringSerde, stringSerde))
                // filter ACTIVE_POWER
                .filter( (String key, String value) -> {
                    JsonParser jp = new JsonParser();
                    JsonObject event = jp.parse(value).getAsJsonObject();
                    String who = event.get("who").getAsString();
                    if(DeviceMappingRegistry.getMapping(com.domosnap.engine.device.counter.PowerCounter.class).equals(who)) {
                        JsonArray whatList = event.getAsJsonArray("what");
                        for (JsonElement what : whatList) {
                            if (what.getAsJsonObject().get("ACTIVE_POWER") != null) {
                                return true; // We get an active value!
                            }
                        }
                    }
                    return false;
                })
                .process(() -> {
                            return new ContextualProcessor<String, String, String, String>() {

                                private long lastCounterTick = 0;
                                private long lastTick = 0;

                                @Override
                                public void init(ProcessorContext<String, String> context) {
                                    super.init(context);
                                    // Add a checking: each XX millisecond, test if value arrived!
                                    context().schedule(Duration.ofMillis(checkDuration), PunctuationType.WALL_CLOCK_TIME, timestamp -> {
                                        lastTick = timestamp;

                                        addressToCheck2.forEach((key, value) -> {
                                            lastCounterTick = value;
                                            if(lastTick - lastCounterTick > durationWithNoNews  && lastCounterTick != 0) {
                                                log.info(Log.Session.OTHER, "Pas de news depuis " + ((lastTick - lastCounterTick)) + "ms. pour le compteur [" + key + "]."); // TODO lever une alerte
                                            } else if (value == 0) {
                                                log.info(Log.Session.OTHER, "Le compteur  [" + key + "] n'a pas encore reçu de message.");
//                                            } else {
//                                                System.out.println("Check: " + key + " done! ");
                                            }
                                        });

//                                        System.out.println("Tick! " + timestamp);
                                    });
                                }

                                @Override
                                public void process(Record<String, String> record) {
                                    if (addressToCheck2.containsKey(record.key())) {
                                        addressToCheck2.put(record.key(), lastTick); // Update lastTick for a counter!
//                                        System.out.println("timestamp: " + lastCounterTick + " / " + record.key() + ":" + record.value());
                                    } else {
//                                        System.out.println("Device non checké : " + record.key());
                                    }
                                }
                            };
                        }
                        , storeName)
        ;

        rule = new KafkaStreams(builder.build(), streamsConfig);
    }

    public KafkaStreams getRule() {
        return rule;
    }
}
