package com.domosnap.vee.rule;

import com.domosnap.engine.Log;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.vee.engine.core.AbstractRule;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.Record;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.time.Instant;
import java.util.*;

public class WaterConsumptionRule extends AbstractRule {

    private static final Log log = new Log(WaterConsumptionRule.class.getName());
    private final KafkaStreams rule;
    public static final String ADDRESS_TO_CHECK = "address.to.check";
    private final long durationWithConsumptionAlert = 1_000*60; // en minute TODO parameter

//    private final long consumption_rate = 10_000; // TODO use parameter

    private final List<String> addressToCheck;

    public WaterConsumptionRule(String name, String bootstrapServer, String topic, Properties settings) {

        super(name, bootstrapServer, topic, settings);

        Object addressToCheck = settings.getOrDefault(ADDRESS_TO_CHECK, new ArrayList<>());
        if (!(addressToCheck instanceof List)) {
            throw new RuntimeException("Bad parameter [" + ADDRESS_TO_CHECK + "]");
        } else {
            this.addressToCheck = (List<String>) addressToCheck;
        }
        Map<String, WaterConsumption> addressToCheck2 = new HashMap<>();

        getAddressToCheck().forEach( address -> addressToCheck2.put(address, new WaterConsumption()) );

        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServer());
        StreamsConfig streamsConfig = new StreamsConfig(props);

        StreamsBuilder builder = new StreamsBuilder();


        String storeName = name + "_store";
        KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore(storeName);
        StoreBuilder<KeyValueStore<String, String>> storeBuilder = Stores.keyValueStoreBuilder(storeSupplier,
                Serdes.String(),
                Serdes.String());
        builder.addStateStore(storeBuilder);

        Serde<String> stringSerde = Serdes.String();

        builder.stream(getTopic(), Consumed.with(stringSerde, stringSerde))
                // filter ACTIVE_POWER
                .filter( (String key, String value) -> {
                    JsonParser jp = new JsonParser();
                    JsonObject event = jp.parse(value).getAsJsonObject();
                    String who = event.get("who").getAsString();
                    if(DeviceMappingRegistry.getMapping(com.domosnap.engine.device.counter.PowerCounter.class).equals(who)) {
                        JsonArray whatList = event.getAsJsonArray("what");
                        for (JsonElement what : whatList) {
                            if (what.getAsJsonObject().get("ACTIVE_POWER") != null) { // Water is the same than power....
                                return addressToCheck2.containsKey(key); // We get an active value!
                            }
                        }
                    }
                    return false;
                })
                .process(() -> {
                            return new Processor<String, String, String, String>() {
                                @Override
                                public void process(Record<String, String> record) {

                                    JsonParser jp = new JsonParser();
                                    JsonObject event = jp.parse(record.value()).getAsJsonObject();
                                    JsonArray whatList = event.getAsJsonArray("what");
                                    for (JsonElement what : whatList) {
                                        if (what.getAsJsonObject().get(PowerCounter.CounterStateName.ACTIVE_POWER.name()) != null) {
                                            String value = what.getAsJsonObject().get(PowerCounter.CounterStateName.ACTIVE_POWER.name()).getAsString();
                                            WaterConsumption consumption = addressToCheck2.get(record.key());
                                            if (value.equals("0")) { // no water consumption
                                                if (consumption.starting_consumption != null) {
                                                    log.info(Log.Session.OTHER, "Consumption stopped. Estimated Consumption: " + consumption.consumption_estimation);
                                                }
                                                // Reset value
                                                consumption.starting_consumption = null;
                                                consumption.present = Instant.now();
                                                consumption.consumption_estimation = 0;
                                            } else if (consumption.starting_consumption == null){ // Consumption starting
                                                // Consumption started!
                                                consumption.starting_consumption = Instant.now();
                                                consumption.present = Instant.now();
                                                consumption.consumption_estimation = Integer.parseInt(value);
                                                log.info(Log.Session.OTHER, "Consumption is starting.");

                                            } else { // Consumption is going
                                                consumption.present = Instant.now();
                                                consumption.consumption_estimation += Integer.parseInt(value);
                                                if (consumption.starting_consumption.plusMillis(durationWithConsumptionAlert).isBefore(consumption.present)) {
                                                    // Alert if consumption is going since more than xx millisecond
                                                    log.info(Log.Session.OTHER, "Consumption is going than more than (" + durationWithConsumptionAlert/1000/60 + " minute). Estimated Consumption: " + consumption.consumption_estimation);
                                                } else {
                                                    log.info(Log.Session.OTHER, "Consumption is going.");
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                        }
                        , storeName)
        ;

        rule = new KafkaStreams(builder.build(), streamsConfig);
    }

    private static class WaterConsumption {

        public Instant starting_consumption;
        public Instant present;

        public int consumption_estimation;
    }

    private List<String> getAddressToCheck() {
        return addressToCheck;
    }

    public KafkaStreams getRule() {
        return rule;
    }
}
