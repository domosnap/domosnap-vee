package com.domosnap.vee.serdes.devices.power;

import com.domosnap.model.devices.power.PowerCounter;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class PowerCounterSerializer implements Serializer<PowerCounter> {

	@Override
	public byte[] serialize(String topic, PowerCounter data) {
		try {

			if (data == null)
				return null;

			return new BinaryMessageEncoder<>(data.getSpecificData(), data.getSchema()).encode(data).array();
		} catch (Exception e) {
			throw new SerializationException("Error when serializing Avro object to byte[] " + e);
		}
	
	}
}
