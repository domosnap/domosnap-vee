package com.domosnap.vee.serdes.devices.power;

import com.domosnap.model.devices.power.PowerCounter;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class PowerCounterDeserializer implements Deserializer<PowerCounter> {

	@Override
	public PowerCounter deserialize(String topic, byte[] data) {

		try {
			if (data == null)
				return null;

			return new BinaryMessageDecoder<PowerCounter>(new SpecificData(), PowerCounter.SCHEMA$).decode(data);
		
		} catch (Exception e) {
			throw new SerializationException("Error when serializing MyCounter to byte[] " + e);
		}
	}

}
