package com.domosnap.vee.serdes.devices.light;

import com.domosnap.model.devices.light.Light;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class LightDeserializer implements Deserializer<Light> {

	@Override
	public Light deserialize(String topic, byte[] data) {

		try {
			if (data == null)
				return null;

			return new BinaryMessageDecoder<Light>(new SpecificData(), Light.SCHEMA$).decode(data);
		
		} catch (Exception e) {
			throw new SerializationException("Error when serializing MyCounter to byte[] " + e);
		}
	}

}
