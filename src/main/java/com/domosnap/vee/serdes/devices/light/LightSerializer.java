package com.domosnap.vee.serdes.devices.light;

import com.domosnap.model.devices.light.Light;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class LightSerializer implements Serializer<Light> {

	@Override
	public byte[] serialize(String topic, Light data) {
		try {

			if (data == null)
				return null;

			return new BinaryMessageEncoder<>(data.getSpecificData(), data.getSchema()).encode(data).array();
		} catch (Exception e) {
			throw new SerializationException("Error when serializing Avro object to byte[] " + e);
		}
	
	}
}
