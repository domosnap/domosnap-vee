package com.domosnap.vee.serdes;

import com.domosnap.model.core.interaction.Command;
import com.domosnap.model.core.interaction.Event;
import com.domosnap.model.core.interaction.Query;
import com.domosnap.model.devices.light.Light;
import com.domosnap.model.devices.power.PowerCounter;
import com.domosnap.vee.serdes.core.*;
import com.domosnap.vee.serdes.devices.light.LightDeserializer;
import com.domosnap.vee.serdes.devices.light.LightSerializer;
import com.domosnap.vee.serdes.devices.power.PowerCounterDeserializer;
import com.domosnap.vee.serdes.devices.power.PowerCounterSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes.WrapperSerde;

public class Serdes {

	static public final class EventSerde extends WrapperSerde<Event> {
		public EventSerde() {
			super(new EventSerializer(), new EventDeserializer());
		}
	}

	static public final class CommandSerde extends WrapperSerde<Command> {
		public CommandSerde() {
			super(new CommandSerializer(), new CommandDeserializer());
		}
	}

	static public final class QuerySerde extends WrapperSerde<Query> {
		public QuerySerde() {
			super(new QuerySerializer(), new QueryDeserializer());
		}
	}

	static public final class PowerCounterSerde extends WrapperSerde<PowerCounter> {
		public PowerCounterSerde() {
			super(new PowerCounterSerializer(), new PowerCounterDeserializer());
		}
	}

	static public final class LightSerde extends WrapperSerde<Light> {
		public LightSerde() {
			super(new LightSerializer(), new LightDeserializer());
		}
	}

	static public Serde<Event> EventSerde() {
		return new EventSerde();
	}

	static public Serde<Command> CommandSerde() {
		return new CommandSerde();
	}

	static public Serde<Query> QuerySerde() {
		return new QuerySerde();
	}

	static public Serde<PowerCounter> PowerCounterSerde() {
		return new PowerCounterSerde();
	}

	static public Serde<Light> LightSerde() {
		return new LightSerde();
	}
}