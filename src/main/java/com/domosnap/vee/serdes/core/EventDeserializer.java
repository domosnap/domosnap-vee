package com.domosnap.vee.serdes.core;

import com.domosnap.model.core.interaction.Event;
import com.domosnap.model.devices.power.PowerCounter;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class EventDeserializer implements Deserializer<Event> {

	@Override
	public Event deserialize(String topic, byte[] data) {
		try {
			if (data == null)
				return null;

			return new BinaryMessageDecoder<Event>(new SpecificData(), PowerCounter.SCHEMA$).decode(data);

		} catch (Exception e) {
			throw new SerializationException("Error when serializing Event to byte[] " + e);
		}
	}

}
