package com.domosnap.vee.serdes.core;

import com.domosnap.model.core.interaction.Query;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class QueryDeserializer implements Deserializer<Query> {

	@Override
	public Query deserialize(String topic, byte[] data) {
		try {
			if (data == null)
				return null;

			return new BinaryMessageDecoder<Query>(new SpecificData(), Query.SCHEMA$).decode(data);

		} catch (Exception e) {
			throw new SerializationException("Error when serializing Query to byte[] " + e);
		}
	}

}
