package com.domosnap.vee.serdes.core;

import com.domosnap.model.core.interaction.Event;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class EventSerializer implements Serializer<Event> {

	@Override
	public byte[] serialize(String topic, Event data) {
        try {

            if (data == null)
                return null;

            return new BinaryMessageEncoder<>(data.getSpecificData(), data.getSchema()).encode(data).array();
        } catch (Exception e) {
            throw new SerializationException("Error when serializing Event to byte[] " + e);
        }

	}
}
