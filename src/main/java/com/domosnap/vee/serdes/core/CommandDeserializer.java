package com.domosnap.vee.serdes.core;

import com.domosnap.model.core.interaction.Command;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class CommandDeserializer implements Deserializer<Command> {

	@Override
	public Command deserialize(String topic, byte[] data) {
		try {
			if (data == null)
				return null;

			return new BinaryMessageDecoder<Command>(new SpecificData(), Command.SCHEMA$).decode(data);

		} catch (Exception e) {
			throw new SerializationException("Error when serializing Command to byte[] " + e);
		}
	}

}
