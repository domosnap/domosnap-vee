package com.domosnap.vee.performance;

import com.domosnap.engine.Log;
import com.domosnap.engine.gateway.Event;
import com.domosnap.engine.gateway.EventJsonCodec;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DevicesEmulator {
    private static final Log log = new Log(DevicesEmulator.class.getSimpleName());

    private final KafkaProducer<String, String> producer;
    private String topic;

    public DevicesEmulator() {

        String bootStrapServer = "PLAINTEXT://localhost:9092";
        this.topic =  "domosnap-event-topic";

        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
        props.put(ProducerConfig.ACKS_CONFIG, "all"); // message passé à tous mes broker "0"= udp, "1"= tcp = récupérer mais pas stocké. Parametre min.insync.replica = 2
        props.put(ProducerConfig.RETRIES_CONFIG, "3");
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100"); // value is for one partition!
        props.put(ProducerConfig.LINGER_MS_CONFIG, "500"); // Waiting time to send even if batch_siz not reached
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432"); //memory for all partition! here: 32Mo (dafault)... Knowing that:  130 octet < one message < 150 octet *100 =>
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<>(props);
    }

    public void accept(final Event event) {

//        ProducerRecord<String, String> myRecord = new ProducerRecord<>(topic, event.getWhere().getURI().toString(), EventJsonCodec.toJSon(event, false));
//        producer.send(myRecord, (metadata, exception) -> {
//            if (exception != null) {
//                log.severe(Log.Session.MONITOR, exception.getMessage());
//            } else {
//                log.finest(Log.Session.MONITOR, "Event send to Kafka with success [" + event + "]");
//            }
//        });
    }

    public void close() throws IOException {
        producer.close();
    }


}
